<?php

class ApiTest extends TestCase
{

    private static $stack;

    public function setUp(): void
    {
        parent::setUp();
    }

    public static function setUpBeforeClass(): void
    {
        $unique_id = time();
        $user = [
            'name' => 'test_user' . $unique_id,
            'email' => 'test_user' . $unique_id . '@test.com',
            'password' => $unique_id,
            'password_confirmation' => $unique_id,
        ];
        self::$stack = [
            'unique_id' => $unique_id,
            'user' => $user
        ];
    }

    public function testRegisterNewUser()
    {
        $this->post(
            '/api/register',
            self::$stack['user']
        )->seeJson(
            [
                'success' => true
            ]
        );
    }

    /**
     * @depends testRegisterNewUser
     */
    public function testRegisterExistingUser()
    {
        $this->post(
            '/api/register',
            self::$stack['user']
        )->seeJson(
            [
                'success' => false
            ]
        );
    }

    /**
     * @depends testRegisterNewUser
     * @return mixed
     */
    public function testUserLogin()
    {
        $result = $this->post(
            '/api/login',
            [
                'email' => self::$stack['user']['email'],
                'password' => self::$stack['user']['password']
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
        return $token = $result->response['data']['token'];
    }

    /**
     * @depends testRegisterNewUser
     */
    public function testFailUserLogin()
    {
        $this->post(
            '/api/login',
            [
                'email' => self::$stack['user']['email'],
                'password' => ''
            ]
        )->seeJson(
            [
                'success' => false
            ]
        );
    }

    /**
     * @depends testUserLogin
     * @param string $token
     */
    public function testAddHeatMapRecord(string $token)
    {
         $this->post(
            '/api/addHeatMapRecord',
            [
                'customer_id' => self::$stack['unique_id'],
                'link' => "/",
                'link_type' => "homepage",
            ],
            [
                'Authorization' => 'Bearer '.$token
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
    }

    public function testAddHeatMapRecordWithoutAuthorization()
    {
          $this->post(
            '/api/addHeatMapRecord',
            [
                'customer_id' => self::$stack['unique_id'],
                'link' => "/",
                'link_type' => "homepage",
            ]
        )->seeJson(
            [
                'success' => false
            ]
        );
    }

    /**
     * @depends testUserLogin
     * @param string $token
     */
    public function testGetLinkHits(string $token)
    {
        $this->post(
            '/api/getLinkHits',
            [
                'link' => "/",
                'from' => date('Y-m-d H:i:s', strtotime("- 1 months")),
                'to' => date('Y-m-d H:i:s'),
            ],
            [
                'Authorization' => 'Bearer '.$token
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
    }

    /**
     * @depends testUserLogin
     * @param string $token
     */
    public function testGetLinkTypesHits(string $token)
    {
        $this->post(
            '/api/getLinkTypesHits',
            [
                'from' => date('Y-m-d H:i:s', strtotime("- 1 months")),
                'to' => date('Y-m-d H:i:s'),
            ],
            [
                'Authorization' => 'Bearer '.$token
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
    }

    /**
     * @depends testUserLogin
     * @param string $token
     */
    public function testGetCustomerJourney(string $token)
    {
        $this->post(
            '/api/getCustomerJourney',
            [
                'customer_id' => self::$stack['unique_id']
            ],
            [
                'Authorization' => 'Bearer '.$token
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
    }

    /**
     * @depends testUserLogin
     * @param string $token
     */
    public function testGetSimilarCustomersByCustomerJourney(string $token)
    {
        $this->post(
            '/api/getSimilarCustomersByCustomerJourney',
            [
                'customer_id' => self::$stack['unique_id']
            ],
            [
                'Authorization' => 'Bearer '.$token
            ]
        )->seeJson(
            [
                'success' => true
            ]
        );
    }
}
