#Heatmap API
- framework used: Lumen

## How to install:
- copy files to webserver
- import the heatmap.sql file to create DB and tables or just create the heatmap database and use php artisan migrate to create the DB structure
- set the DB credentials in the .env

## How to test:
Use the Heatmap.postman_collection file to load the Heatmap collection into Postman
1. Add global variables: jwt and endpoint (set with the endpoint where the API will be available)
2. Register a user in order to use the API, API requires JWT Auth
3. Login with the user in order to get the token
4. Call addHeatMapRecord to add a new entry in the heatmap table
5. Call getLinkHits to get all the link hits in a provided time interval
6. Call getLinkTypesHits to get all the link types hits in a provided time interval
7. Call getCustomerJourney to get all the accessed links by the requested customer
8. Call getSimilarCustomersByCustomerJourney to get all customers with similar Journey like requested customer

### Enjoy!
