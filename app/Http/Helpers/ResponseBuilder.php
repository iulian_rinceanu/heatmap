<?php


namespace App\Http\Helpers;


class ResponseBuilder
{
    /**
     * @param $data
     * @param bool $status
     * @return \Illuminate\Http\JsonResponse
     */
    public static function result($data, $status = true)
    {
        return response()->json([
                "success" => $status,
                "data" => $data,
            ], $status?200:409
        );
    }
}
