<?php

namespace App\Http\Controllers;


use App\Http\Helpers\ResponseBuilder;
use App\Models\Heatmap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeatmapController extends Controller
{
    /**
     * Instantiate a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addHeatMapRecord(Request $request)
    {
        list($data, $status) = [null, false];

        try {
            $this->validate(
                $request,
                [
                    'customer_id' => 'required|int',
                    'link' => 'required|string',
                    'link_type' => 'required|string',
                ]
            );

            $heatmap = new Heatmap;
            $heatmap->customer_id = $request->input('customer_id');
            $heatmap->link = $request->input('link');
            $heatmap->link_type = $request->input('link_type');
            $heatmap->save();

            $data = $heatmap;
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLinkHits(Request $request)
    {
        list($data, $status) = [null, false];

        try {
            $this->validate(
                $request,
                [
                    'link' => 'required|string',
                    'from' => 'required|string',
                    'to' => 'required|string',
                ]
            );

            $data = Heatmap::where("link", $request->input('link'))
                ->where('timestamp', '>=', $request->input('from'))
                ->where('timestamp', '<=', $request->input('to'))
                ->count();
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLinkTypesHits(Request $request)
    {
        list($data, $status) = [null, false];

        try {
            $this->validate(
                $request,
                [
                    'from' => 'required|string',
                    'to' => 'required|string',
                ]
            );

            $data = Heatmap::select('link_type', DB::raw('COUNT(0) as hits'))
                ->where('timestamp', '>=', $request->input('from'))
                ->where('timestamp', '<=', $request->input('to'))
                ->groupBy('link_type')
                ->get();
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerJourney(Request $request)
    {
        list($data, $status) = [null, false];

        try {
            $this->validate(
                $request,
                [
                    'customer_id' => 'required|int'
                ]
            );

            $data = Heatmap::select('link')
                ->where('customer_id', $request->input('customer_id'))
                ->orderBy('timestamp')
                ->get();
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSimilarCustomersByCustomerJourney(Request $request)
    {
        $ref_customerJourney = [];
        list($data, $status) = [null, false];

        try {
            $this->validate(
                $request,
                [
                    'customer_id' => 'required|int'
                ]
            );

            $rawData = Heatmap::select('customer_id', DB::raw('GROUP_CONCAT(link) as links'))
                ->groupBy('customer_id')
                ->get();
            $status = true;

            $processedData = [];

            foreach ($rawData as $row) {
                if ($request->input('customer_id') == $row['customer_id']) {
                    $ref_customerJourney = explode(",", $row['links']);
                } else {
                    $processedData[$row['customer_id']] = explode(",", $row['links']);
                }
            }

            $data = array_keys(
                array_filter(
                    $processedData,
                    function ($entry) use ($ref_customerJourney) {
                        return count(array_intersect($entry, $ref_customerJourney)) == count($entry);
                    }
                )
            );
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }
}
