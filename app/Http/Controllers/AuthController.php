<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ResponseBuilder;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\JWTAuth;


class AuthController extends Controller
{
    /**
     * @var JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        list($data, $status) = [null, false];

        try {
            //validate incoming request
            $this->validate(
                $request,
                [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed',
                ]
            );

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);

            $user->save();
            $data = $user;
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        list($data, $status) = [null, false];

        try {
            //validate incoming request
            $this->validate(
                $request,
                [
                    'email' => 'required|string',
                    'password' => 'required',
                ]
            );

            $credentials = $request->only(['email', 'password']);

            if (!$token = Auth::attempt($credentials)) {
                throw new \Exception("Unauthorized");
            }

            $data = $this->respondWithToken($token);
            $status = true;
        } catch (\Exception $e) {
            $data = $e;
        } finally {
            return ResponseBuilder::result($data, $status);
        }
    }
}
