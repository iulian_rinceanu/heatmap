<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// API route group
$router->group(
    ['prefix' => 'api'],
    function () use ($router) {
        $router->post('register', 'AuthController@register');
        $router->post('login', 'AuthController@login');

        $router->post('addHeatMapRecord', 'HeatmapController@addHeatMapRecord');
        $router->post('getLinkHits', 'HeatmapController@getLinkHits');
        $router->post('getLinkTypesHits', 'HeatmapController@getLinkTypesHits');
        $router->post('getCustomerJourney', 'HeatmapController@getCustomerJourney');
        $router->post('getSimilarCustomersByCustomerJourney', 'HeatmapController@getSimilarCustomersByCustomerJourney');
    }
);
